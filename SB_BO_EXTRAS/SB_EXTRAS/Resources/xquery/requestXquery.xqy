xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare default element namespace "";
(:: import schema at "../requestRunFunction.xsd" ::)
declare namespace ns1="https://www.gabriellispa.it/bo/toshiba/utilities/types";
(:: import schema at "../../Business/XMLSchema_1087730185.xsd" ::)

declare variable $restInput as element() (:: schema-element(request) ::) external;

declare function local:func($restInput as element() (:: schema-element(request) ::)) as element() (:: element(*, ns1:inputType) ::) {
    <ns1:inputType>
        <ns1:P_OPEID>{fn:data($restInput/P_OPEID)}</ns1:P_OPEID>
        <ns1:P_RUOLOID>{fn:data($restInput/P_RUOLOID)}</ns1:P_RUOLOID>
        <ns1:P_DECORRENZA>{fn:current-date()}</ns1:P_DECORRENZA>
        <ns1:P_PDVCOD>{fn:data($restInput/P_PDVCOD)}</ns1:P_PDVCOD>
        <ns1:P_FUNZIONECOD>{fn:data($restInput/P_FUNZIONECOD)}</ns1:P_FUNZIONECOD>
        <ns1:P_VALUE>{fn:data($restInput/P_VALUE)}</ns1:P_VALUE>
    </ns1:inputType>
};

local:func($restInput)
